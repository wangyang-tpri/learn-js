/**
 * async函数一定会返回一个promise对象，如果一个函数的返回值不像promise对象的话，那么它将被隐式的包装在一个promise对象中
 * await表达式会暂停整个async函数的执行进程并让出其控制权
 */

let foo = async () => {
    let result1 = await new Promise((resolve) => {
        setTimeout(() => { resolve(2) }, 2000)
    })
    let result2 = await new Promise((resolve) => {
        setTimeout(() => { resolve(10)}, 1000)
    })
    console.log( result1 )
    console.log( result2 )
}
foo()
foo().catch(() =>{/**错误处理 */})
