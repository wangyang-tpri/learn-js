/**
 * 手写绘制echarts的折线图 模块
 * 初始化时，电脑屏幕的左上角 就是起始点(0, 0)
 *  1.需要先绘制出echarts图表的坐标系 x轴 和 y轴
 *  2.绘制轴线
 *  3.在画布上面绘制折线图
 */
(function (window) {
    /**
     * 
     * @param { object } opt 绘制echarts图标的构造函数中的有关canvas容器的属性的参数 
     */
    function Echarts(opt) {
        this.ele = opt.ele;
    }
    function coordinateSystem(ele) {
        var marginBottom = 50
        var marginLeft = 100
        ele.width = 500
        ele.height = 300
        var ctx2d = ele.getContext('2d')
        ctx2d.strokeStyle = 'black'
        ctx2d.lineWidth = 1
        // 其实的坐标系是相对于屏幕的左上角的
        // 对坐标系进行转换， 此时的坐标轴 变成容器的左下角
        ctx2d.scale(1, -1) 

        ctx2d.translate(marginLeft, -ele.height + marginBottom)
        ctx2d.save()  // 保存当前的绘图状态 每个状态随时存储canvas的上下文数据  
        ctx2d.moveTo(0, 0)
        ctx2d.lineTo(0, 200)
        ctx2d.stroke()  

        var heightOne = 30;
        drawXisLine( ctx2d,ele, heightOne )
        // 这个restore居然影响坐标轴绘制折线图时的展示情况
        ctx2d.restore()  // 恢复之前的绘图状态
        ctx2d.save()
        drawText( ctx2d, heightOne )
        ctx2d.restore()
        var data = dataArr( ele, marginLeft)
        drawLine(data, ctx2d)
    }
    function drawXisLine(ctx2d, ele, heightOne ) {
        for (var i = 0; i < 7; i++) {
            ctx2d.moveTo(0, 0)
            ctx2d.lineTo(ele.width, 0)
            ctx2d.closePath()
            ctx2d.stroke()
            ctx2d.translate(0, heightOne)
        }
        ctx2d.restore()
        ctx2d.save()
        var xAxis = 60
        for (var i = 0; i < 8; i++) {
            ctx2d.beginPath()
            ctx2d.moveTo(0, 0)
            ctx2d.lineTo(0, -5)
            ctx2d.closePath()
            ctx2d.stroke()
            ctx2d.translate(xAxis, 0)
        }
    }
    function createData(xNum, yNum) {
        var obj = {};
        obj.x = xNum
        obj.y = yNum
        return obj
    }
    function drawText( ctx2d, heightOne ){
        var text = ['周一', '周二', '周三', '周四', '周五', '周六', '周七']
        ctx2d.scale(1, -1)
        for (var i = 0; i < 8; i++) {
            ctx2d.stroke()
            if (i == 0) {
                ctx2d.translate(heightOne / 2, 20)
            } else {
                ctx2d.translate(60, 0)
            }
            ctx2d.fillText(text[i], 0, 0)
        }
        ctx2d.restore()
        ctx2d.scale(1, -1)
        for (var i = 0; i < 7; i++) {
            ctx2d.stroke()
            ctx2d.fillText((50 * i).toString(), -30, 0)
            ctx2d.translate(0, -heightOne)
        }
    }
    // 自己初始化了一部分数据
    function dataArr(ele, marginLeft){
        var xCenter = (ele.width - marginLeft) / 6.8;
        var unitHeight = 35/50;
        var data1 = createData(xCenter / 2, 150 );
        var data2 = createData(xCenter / 2 + xCenter, 200*unitHeight);
        var data3 = createData(xCenter / 2 + xCenter * 2, 130*unitHeight);
        var data4 = createData(xCenter / 2 + xCenter * 3, 110*unitHeight);
        var data5 = createData(xCenter / 2 + xCenter * 4, 140*unitHeight);
        var data6 = createData(xCenter / 2 + xCenter * 5, 260*unitHeight);
        var data7 = createData(xCenter / 2 + xCenter * 6, 50*unitHeight);
        var data = [data1, data2, data3, data4, data5, data6, data7];
        return data;
    }
    function drawLine(data, ctx) {
        ctx.beginPath()
        for (var i = 0; i < data.length; i++) {
            ctx.lineTo(data[i].x, data[i].y)
        }
        ctx.strokeStyle = 'red'
        ctx.stroke()
        ctx.closePath()
        // ctx.restore()
        ctx.beginPath()
        ctx.moveTo(0, 0)
        // 绘制封闭的折现面积图
        for ( var i = 0; i < data.length; i++) {
            ctx.lineTo(data[i].x, data[i].y)
            
        }
        ctx.lineTo(data[data.length - 1].x, 0)
        ctx.lineTo(0, 0)
        ctx.closePath()
        ctx.fillStyle = 'rgb(50, 30, 150)'
        ctx.fill()
        ctx.restore()
    }
    Echarts.prototype = {
        constructor: Echarts,
        init: function () {
            this.drawCanvas(this.ele)
        },
        drawCanvas: function (ele) {
            coordinateSystem(ele)
        }
    }
    window.echarts = {
        init: function (opt) {
            return new Echarts(opt).init()
        }
    }


})(window)

