/*
 * @Author: wangyang-tpri
 * @Date: 2021-12-28 14:15:41
 * @LastEditTime: 2021-12-28 21:10:30
 * @email: angularyang@163.com
 * @description: es6数组中新增的扩展
 */

/**
 * 正则表达式是一种用来匹配字符串的强有力的武器
 *     字符串的方法：match matchAll search replace split
 *     正则表达式的方法：exec test
 */

// 字面量创建
const re = /\d+/g;
// RegExp构造函数创建
const re2 = new RegExp('\\d+', 'g');
let numStr = '12345';
const reg = /(\d{1,3})(\d{1,3})/;
console.log(numStr.match(reg));
/**
 * exec 返回字符串str中的regexp的匹配项 它是在正则表达式中调用 而不是在str中调用
 */
let regexp = /javascript/ig;
let str = 'javascript is a very nice lanunch , javascript';
function getStrIndex(str){
    let result;
    while(result = regexp.exec(str)){
        console.log( result )
        console.log(`${result[0]} 在${str}中的位置是${result.index}`)
    }
}
getStrIndex(str)

/**
 * 使用正则表达式作为分隔符来分割字符串
 */
console.log('12, 34, 56'.split(/,\s*/));

// 展开语法
let arr = ['js', 'css', 'html'];
console.log( ...arr)
let o = {
    n: '消防',
    age: 18
}
let dd = {...o, ...arr };
console.log(dd)
// Symbol 是一es6中新增的一个数据类型 翻译为符号 用来生成一个独一无二的值
// 防止属性名冲突
// Set Map es6新增的数据结构 Set中的元素不会重复的，可以利用set去重
let list = [1, 2, 3, 4, 3, 2];
console.log( new Set( list ) )
new Set(list).delete( 2 )
let arrSet = [...new Set(list)];
console.log( arrSet )

// map是可以将一个对象作为一个对象的key的

// es7 
// array includes 判断数组中是否包含指定的元素
let arri = ['js', 'html', 'css']
console.log( arri.includes('css', 3))  // 3表示指定开始位置的索引 包含当前索引位置的值
// math.pow 指数乘方
console.log( Math.pow(4, 4))
// object.values()获取对象中所有的value
let oo = {
    name: '小明',
    age: 20
}
console.log( Object.values(oo))

// object.entries可以获取到一个数组，数组中存放可枚举属性的键值对数组
console.log( Object.entries(oo))

// flat flatMap
let arrD  =[9 , [[30, 50], 0], [20]];
console.log( arrD.flat(Infinity) )  // 无论有多少层  都会深度递归遍历数组

console.log( globalThis )
let str0 = '  hello world '
console.log( str0.trim())
console.log( str0.trimEnd())

// 逻辑分配运算符 es12
// ||= 逻辑或
// &&= 逻辑与
// ??= 逻辑空
// let message = null;
// // message ??= 'default value';
// console.log( message )
// let info = {
//     name: 'xiao'
// }
// info &&= info.name;
// console.log( info )

let p = 'hello world and world';
console.log( p.replace('world', 'fang'))

// require属于运行时调用 import 属于编译时调用
// require属于同步执行  import 属于异步执行

class Dog {
    constructor(name){
        this.name = name
    }
    sayName(){
        console.log( this.name )
    }
}
let d1 = new Dog('today');
d1.sayName();