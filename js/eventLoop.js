/*
 * @Author: wangyang-tpri
 * @Date: 2021-12-28 21:16:52
 * @LastEditTime: 2021-12-28 21:49:10
 * @email: angularyang@163.com
 * @description: js中的事件循环
 */
/**
 * js中的事件分为 同步和异步事件，更准确的分类的话 应该是 宏任务 微任务
 *  执行的先后顺序一定是 宏任务 -- 微任务 -- 宏任务 这样的一个先后顺序和循环
 * javascript是单线程的语言，意味着在同一时间内只能做一件事情，但是这并不意味着单线程就是阻塞，实现单线程非阻塞的方法就是事件循环
 */

console.log(1) 
/**
 * 定时器属于宏任务 放着先不执行
 */
setTimeout(() => {
    console.log('setTimeout')
}, 0)
new Promise((resolve, reject) => {
    console.log('new Promise')
    resolve();
}).then(() => {

    console.log('then')
}
)
console.log(2)

// 打印结果的顺序是：1 -- new promise -- 2 -- then -- setTimeout

async function fn1(){
    console.log('async1')
    await fn2();  // await一定会阻塞后面的执行 所以后面的执行操作就会放到 微任务队列里面去
    console.log('async2')
}
async function fn2 (){
    console.log('fn2')
}
 fn1();
console.log('async3')
