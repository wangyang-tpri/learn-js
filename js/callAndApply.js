/*
 * @Author: wangyang-tpri
 * @Date: 2021-12-28 08:48:01
 * @LastEditTime: 2021-12-28 09:42:00
 * @email: angularyang@163.com
 * @description: call和apply的用法
 */

/** 
 * call/apply/bind都是为了改变函数的执行上下文，也就是改变函数的this指向
 *  call/apply都是临时改变，都会立即执行函数调用的
 *      call传入的是参数 apply传入的是参数是数组
 *  bind返回的是绑定this之后的函数、call和apply则会立即执行
 *  非严格模式下，如果第一个参数是null或者undefined的话 ，this便会指向window
 *  严格模式下，如果第一个参数是null或者undefined的话，this便会指向undefined
*/

let name = 'lucky';
let obj = {
    name: 'margin',
    say: function () {
        console.log(this.name)
    }
}

obj.say(); // margin
setTimeout(obj.say, 0) // 此时的执行上下文是全局上下文，所以this指向window
setTimeout(obj.say.bind(obj), 0) //此时通过bind方法改变函数的执行上下文 this指向obj

//手写bind方法
Function.prototype.myBind = function(context){
    if (typeof this != 'function') {
        throw new TypeError('调用对象不是函数')
    }
    const args = [...arguments].slice(1),
        fn = this;
    return function Fn(){
        return fn.apply(this instanceof Fn ? new fn(...arguments) : context, args.concat(...arguments));
    }
}
let obj2 = {
    name: 'tpri'
}
function fn(...args){
    console.log(this, args)
}
myBindFn = fn.bind(obj2)
myBindFn(1,2)
myBindFn2 = fn.myBind(obj2)
myBindFn2(1,2)

