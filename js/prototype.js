/*
 * @Author: wangyang-tpri
 * @Date: 2021-12-30 15:16:45
 * @LastEditTime: 2021-12-30 15:45:32
 * @email: angularyang@163.com
 * @description: js中的原型和原型链
 */

/**
 * 每个函数都有一个特殊的属性叫做原型 prototype
 */
function doSomeThing(){}
/** 原型对象有一个自有属性叫 constructor 这个属性指向该函数*/
console.log( doSomeThing.prototype )
console.log(doSomeThing.prototype.constructor)  // function doSomeThing(){}

/**
 * 原型对象也可能拥有原型，并从中继承方法和属性，一层一层，以此类推，这种关系常被称为原型链（prototype chain）
 * 对象实例和它的构造器之间建立一个连接通过 __proto__ 属性，它是通过prototype属性派生的，之后通过上溯原型链，在构造器中找到这些属性和方法
 */

function Person(name, age){
    this.name = name;
    this.age = age;
}
let person = new Person('a', 25);
console.log(person.__proto__ == Person.prototype)
console.log(Person.__proto__ == Function.prototype)  // Person是个函数对象，默认由Function作为类创建
/**
 * Person.prototype.__proto__ 指向内置对象，因为Person.prototype是个对象
 * 默认是由Object作为类创建的，而Object.prototype为内置对象
 */
console.log(Person.prototype.__proto__ == Object.prototype )
/**
 * 同时指向内置匿名函数anonymous，这样原型链的终点就是null
 */
console.log(Function.prototype == Function.__proto__)