/*
 * @Author: wangyang-tpri
 * @Date: 2021-12-30 15:54:19
 * @LastEditTime: 2021-12-30 21:41:37
 * @email: angularyang@163.com
 * @description: js中的继承
 */
/**
 * js中的继承一般有以下几种
 *  原型链继承             缺点:属性公用一个内存地址，如果一个子类修改的话，会影响其他的。原因：2个实例使用的是同一个原型对象，内存空间是共享的。
 *  构造函数继承(借助call)  缺点:父类原型对象中一旦存在父类之前定义的方法，那么子类将无法继承这些方法
 *  组合继承 将原型继承和 构造函数的继承的优点合并到一起
 *  原型式继承
 *  寄生式继承
 *  寄生组合式继承
 */


/**组合继承 */
function Parent1(){
    this.type = 'parent1'
    this.arr = [1, 2, 3]
}
Parent1.prototype.sayType = function(){
    console.log(this.type, this.arr)
}
function Child1(){
    Parent1.call(this)
    this.type = 'child1';
}
Child1.prototype = new Parent1();
let child2 = new Child1();
let child3 = new Child1();
child2.arr.push(5)
child3.arr.push(6)
child2.sayType();
child3.sayType();

/**原型式继承
 * 
 * 主要是利用Object.create()实现普通对象的继承
 *  也是实现了浅拷贝 共用了一个内存地址 和原型链继承有着同样的缺点
 */
let Parent2 = {
    arr: [1,3],
    sayName: function(){
        console.log(this.arr)
    }
}
let Child4 = Object.create(Parent2);
let c5 = Object.create(Parent2);
Child4.arr.push(5);
c5.sayName();

/**
 * 寄生式组合继承 也是利用了Object.create()的浅拷贝的能力，在此基础上再添加一些方法
 */

/**
 * 最优的继承 方法是 寄生式组合继承
 *      es6中的继承实际上使用的就是 寄生式组合继承的方式
 */
function clone(parent, child){
    child.prototype = Object.create(parent.prototype);
    child.prototype.constructor = child;
}
function P6 (){
    this.name = 'p6';
}
P6.prototype.sayName = function(){
    console.log(this.name)
}
function C1(){
    P6.call(this);
    this.name = 'c1';
}
clone(P6, C1)
let c2 = new C1();
c2.sayName();
