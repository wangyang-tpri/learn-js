import { createRouter, createWebHashHistory } from 'vue-router'
import self from '@/components/self.vue'
import theWelcome from '@/components/TheWelcome.vue'
import setUp from '@/components/setUp.vue'
import Attribute from '@/components/Attribute.vue'
import LifeCicle from '@/components/LifeCicle.vue'
import WatchD from '@/components/WatchD.vue'
const routes = [{
    path: '/', component: () => import('@/components/self.vue')
},{
    path: '/self', component: self
}, {
    path :'/wel', component: theWelcome
}, {
    path: '/setUp', component: setUp
}, {
    path: '/attr', component: Attribute
}, {
    path: '/life', component: LifeCicle
}, { 
    path: '/watch', component: WatchD
}]
 export default createRouter({
    history: createWebHashHistory(),
    routes
})