/**
 * 使用pinia进行状态管理
 * state 中定义基本的数据 function
 * getters 相当于一个computed 对数据进行缓存处理 Object
 * actions 相当于一个methods 对数据进行状态变更  Object
 * 可以直接在单文件组件中使用
 * 通过defineStore来进行定义
 * 单文件组件中
 */
import { defineStore } from "pinia";

export const counterStore = defineStore('count', {
    state: () => ({ count: 20}),
    getters: {
        double: (state) => {
            return state.count*2
        }
    },
    actions: {
        increment1: function() {
            this.increment3()
        },
        increment3: function() {
            this.count++
        },
        increment4: () => {
            console.log( this )
        }
    }
})