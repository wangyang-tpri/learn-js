import { createApp } from 'vue'
import { createPinia } from 'pinia'
import Vue from 'vue'
import App from './App.vue'
import router  from './router'
import './assets/main.css'
import wlBimViewer from 'wl-bim-viewer';
import "wl-bim-viewer/lib/wl-bim-viewer.css"
const app = createApp(App)
const pinia = createPinia()
app.use(router)
app.use( pinia )
app.mount('#app')
