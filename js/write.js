let a = [20, 30, 9, 10, 9, 20];
let b = a.filter( e => e > 10 )
console.log(b, a)
let c = a.filter((v, i, a) => {
	return a.indexOf(v) === i;
})
//不改变原数组
console.log(a, c)

//数组扁平化
let d = [2, 1, [3]];

let bianpinghua = (arr) => {
	let result = [];
	for (let i = 0; i < arr.length; i++) {
		if (Array.isArray(arr[i])) {
			result = result.concat(bianpinghua(arr[i]))
		} else {
			result.push(arr[i]);
		}
	}
	return result;
}
console.log(bianpinghua(d))
let f = d.concat(a)
console.log(d, f, a)
// concat也是不改变原数组
// instanceof是用来判断构造函数的prototype属性是否出现在某个实例对象的原型链上
// object instanceof constructor
class Animal {
	constructor(name) {
		this.name = name
	}
	getName() {
		return this.name
	}
}
class Dog extends Animal {
	constructor(name, age) {
		super(name)
		this.age = age;
	}
}
let g = new Dog();
g.name = '333'
console.log(g.getName())
let someArr = [ 20, 50, [ 2, 3, 5], 90]
let flatten = (arr) => {
	while (arr.some(v => Array.isArray(v))) {
		arr = [].concat(...arr)
	}
	return arr
}
console.log( [].concat( ...someArr ) )
let h = [20, 30, 50];
let fIndex = (arr) => {
	let result = 0;
	result = arr.findIndex((v) => { return v > 30 }
	)
	return result;
}
//不会改变原数组
console.log(fIndex(h))
//模板字符串
let render = (template, data) => {
	let reg = /\{\{(\w+)\}\}/;
	if (reg.test(template)) {
		console.log(reg.exec(template));
		const name = reg.exec(template)[1];
		template = template.replace(reg, data[name]);
		return render(template, data);
	}
	return template;
}
let template = `我是{{name}}, 年龄{{age}}`;
let person = {
	name: 'ky',
	age: 12
}
console.log(render(template, person))

//函数防抖 事件 n秒后执行一次
//函数节流 时间 n秒内只会执行一次
/**函数柯里化 */
let curry = (fn) => {
	console.log(fn.length)
	let judge = (...args) => {
		if (args.length == fn.length) return fn(...args)
		return (...arg) => judge(...args, ...arg)
	}
	return judge;
}
function add(a, b, c) {
	return a + b + c
}
let addCurry = curry(add)
console.log(addCurry)
const log = (...args) => {
	console.log(args)
}
log(1, 2, 3)
function sum(x) {
	return function (y) {
		return x + y;
	}
}
console.log(sum(2)(3))

//闭包  内部函数可以调用外部作用域的变量
let cosur = () => {
	let count = 0;
	let cc = () => {
		count++;
	}
	cc();
	console.log(count, 'cosur' )
}
cosur();
console.log(Number.MAX_SAFE_INTEGER)
console.log(Number.MIN_SAFE_INTEGER)
// 函数提升 优先于 变量提升  
//装箱和拆箱
// valueof 和 
// 横向匹配 和 纵向匹配
// 贪婪匹配 和 惰性匹配
let reg = /ab{2,5}d/g;
let reg2 = /a[123]b/g;
let reg3 = /[1-6a-fG-M]/;

let reg4 = /goodBye|good/;
let str4 = 'goodBye';
console.log(str4.match(reg4));
let regColor = /#([0-9a-fA-F]{6}|[0-9a-fA-F]{3})/;
let regTime = /^([01][0-9]|[2][0-3]):[0-5][0-9]$/;
console.log(regTime.test('23:56'))
console.log(regTime.test('14:23'))
let regDate = /^[0-9]{4}-([0][1-9]|[1][0-2])-(0[1-9]|[12][0-9]|3[01])$/;
console.log(regDate.test('1991-02-05'))


//分支结构也是惰性的 即当前面的匹配上了 后面的就不再尝试了

//切分 提取 
let splitDate = (date) => {
	let reg = /\D/;
	return [].concat(date.split(reg));
}
let str1 = '1991-02-05';
console.log(splitDate(str1))
//修饰符 表达式 元字符 量词 脱字符 ^ 相当于求反[^\d]
//i g m
// [] | {}
// \d \w \D \W
// n+ n* n?