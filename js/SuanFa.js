//双指针：去除数组中的重复元素，并返回新数组的长度
let arr = [0, 1, 2, 3, 3, 4, 4, 5];
let removeDuplicates = (arr) => {
    let i = 0;
    if (arr.length == 0) {
        return i;
    }
    for (let j = 1; j < arr.length; j++) {
        if (arr[j] != arr[i]) {
            i++;
            arr[i] = arr[j]
        }
    }
    return i + 1;
}
console.log(removeDuplicates(arr))

//寻找数组的中心下标 就是当前数组的下标的左侧元素的和等于右侧元素的和
let arr2 = [1, 7, 3, 8, 0];
let pivotIndex = (arr) => {
    let sum = 0, total = 0;
    sum = arr.reduce((prev, curr) => prev + curr);
    for (let i = 0; i < arr.length; i++) {
        total += arr[i]
        if (total == sum) {
            return i;
        }
        sum -= arr[i];
    }
    return -1;
}
console.log(pivotIndex(arr2))

//二分查找法
let binarySearch = (x) => {
    let index = -1;
    let l = 0;
    let r = x;
    while (l <= r) {
        let mid = parseInt(l + (r - l) / 2);
        if (mid * mid <= x) {
            index = mid;
            l = mid + 1;
        } else {
            r = mid - 1;
        }
    }
    return index;
}
console.log(binarySearch(20))

//链表反转
let iterate = (listNode) => {

}
//三个数的最大乘积  线性扫描
let productA = (arr) => {
    // 升序排列
    arr.sort((a, b) => a - b)
    let n = arr.length;
    let max = Math.max(arr[0] * arr[1] * arr[n - 1], arr[n - 1] * arr[n - 2] * arr[n - 3]);
    return max;
}
console.log(productA(arr = [10, 20, 5, 70, 9]))
// 数组中两数之和

//二分查找  
/**
 * 有序数组 target = 10  返回符合条件的下标
 */

let towSearch = (arr, target) => {
    let searchArr = [];
    for (let i = 0; i < arr.length; i++) {
        let l = i, r = arr.length - 1;
        // l = 0 r = 4;
        // 如果中点的小于 目标值的话
        // 那么左边的只会更小 左边的不要检索了，只需要找右边的即可。
        while (l <= r) {
            let mid = parseInt(l + (r - l) / 2);
            // mid = 2;
            if (arr[mid] == target - arr[i]) {
                searchArr.push(i, mid);
                return searchArr;
            } else if (arr[mid] < target - arr[i]) {
                l = mid + 1;
                // l = 3;
            } else {
                r = mid - 1;
            }
        }
    }
    return [];
}
let arrs = [3, 5, 7, 10, 15], target = 17;

console.log(towSearch(arrs, target))
/**
 * 
 * @param {} arr 数组 
 * @param {*} target  目标值
 * @description 使用双指针求数组中的解 对于这种有序排列 双指针对数组的操作确实是非常的有效和简洁
 */
let twoPoint = (arr, target) => {


  // git diff 命令
    let i = 0, j = arr.length - 1, result = [];
    while (i < j) {
        let sum = arr[i] + arr[j];
        if (sum == target) {
            result.push(i, j)
            return result;
        } else if ( sum > target) {
            j --;
        } else {
            i ++;
        }
    }
    return result;
}
console.log(twoPoint(arrs, target))
/**
 * 贪心算法 求最长的连续递增序列
 */
let findIndex = (arr) => {
    let result = 0;
    let max = 0;
    let start = 0;
    for (let i = 1; i < arr.length; i++) {
        if( arr[i] <= arr[i-1]) {
            start = i;
        }
        max = Math.max(max, i - start + 1)
    }
    return max;
}
let arrI = [1,2,6, 5,6,7]
console.log(findIndex(arrI))
/**
 * 贪心算法 柠檬水找零 局部求最优解 不影响整体的最优解决方 达到整体最优
 */
let changeLemon = (arr) => {
    let five = 0, ten = 0;
    let result = true;
    for( let i = 0 ; i < arr.length; i ++) {
        if ( arr[i] == 5) {
            five ++;
        } else if (arr[i] == 10) {
            if( five =  0) {
                return false;
            } 
            five --;
            ten ++;

        } else {
            if( five > 0 && ten > 0) {
                five --;
                ten --;
            } else if ( five >=3 ) {
                five -= 3;
            } else {
                return false;
            }

        }
    }
    return result;
}
console.log( changeLemon( arr = [5, 5, 10,5,10, 20]))

/**
 * 以时间换空间
 */
let arr20 = [10, 2, 4, 6]
let taraget = 6;
let searchIndex = (target, arr) => {
    let result = [];
    let temArr2 = [];
    for(let i = 0, j = arr.length; i < j; i++){
        if(temArr.length < 1) {
            temArr.push(arr[i])
            // 测试切换分支后的效果
        } else {
            let index = temArr.indexOf(target - arr[i]) ;
            if(index == -1) {
                temArr.push(arr[i])
            } else {
                result.push([index, i])
                break;
            }
        }
        
    }
    console.log(result)
    return result[0]
     
}
// console.log((taraget, arr20))

/**排序算法 */
/**选择排序  */
// 未排序空间内的最小元素

/**
 * 冒泡排序 通过连续的比较于交换相邻元素实现排序
 * 
 * */
let bubbing = (arr) => {
    let len = arr.length;
    for(let i = 0; i < len -1; i++) {
        // 内循环
        let flag = false;
        for(let k = 0; k < len - 1 - i; k++) {
            if(arr[k] > arr[k + 1]) {
                let tem = arr[k]
                arr[k] = arr[k + 1]
                arr[k + 1] = tem
                flag = true
            }
        }
        if(!flag) {
            break
        }
    }
}
bubbing(arr20)
console.log(arr20)
/**
 * 选择排序 
 * 
 * */
let selectSort = (arr) => {
    let len = arr.length;
    for(let i = 0; i < len -1; i++) {
        let temI = i;
        // 内层循环
        for(let k = i + 1; k < len; k++) {
            if(arr[k] > arr[temI]) {
                temI = k
            }
        }
        let tem = arr[i]
        arr[i] = arr[temI]
        arr[temI] = tem
    }
}
selectSort(arr20)
console.log(arr20)

/**
 * 插入排序  
 * 在未排序区间选择一个基准元素，将该元素与左侧已排序的元素逐一比较大小，并将该元素插入到正确的位置上
 * 
 * */

let insertSort = (arr, target) => {
    
    arr = bubbing(arr)
    let len = arr.length;
    for(let i = 1; i < len; i++) {
        let base = arr[i], j = i -1;
        while(j >= 0 && arr[j] > base) {

        }
        arr[i] = base
    }
}


// 贪心算法
/**
 *
 * 零钱兑换 在问题的每个决策阶段 都选择当前看起来最优的选择 
 * 
 * */
let greedyFunc = (number, arr) => {
    let result = 0;
    let len = arr.length -1;
    // 循环进行贪心选择 直到无剩余金额
    while(number > 0) {
        // 找到最接近且小于剩余金额的硬币
        while(arr[len] > number && len > 0) {
            len --
        }
        number -= arr[len]
        result ++     
    }
    return number == 0 ? result : -1;
}
let arrN = [1, 2, 5, 10, 20, 50, 100]
let result2 = greedyFunc(129, arrN)
console.log(result2)

/**
 * 最大容量问题  贪心算法
 * 计算2个短板之间的最大容量
 */
let maxCapacity = (arr) => {
    /**
     * 初始状态下， 指针i和j分别为数组的两端
     * 计算当前容量，更新最大值
     * 比较i和j的大小，向短板一侧移动
     * 循环比较进行2、3步，知道i和j相等为止
     */
    let i = 0, j = arr.length -1, res = 0;
    // 循环贪心算法 直到2扳相遇为止
    while(i < j) {
        res = Math.max(Math.min(arr[j] ,arr[i]) * (j - i), res)
        if(arr[i] < arr[j]) {
            i++
        } else {
            j--
        }
    }
    return res;
}
console.log(maxCapacity(arrN))
/**
 * 最大切分乘积
 * 
 */
let maxProduct = (number) => {
    let a = Math.floor(number / 3)
    let b = number % 3;
    if( b == 1) {
        return Math.pow(3, a-1) * 2 * 2
    } 
     if (b == 2) {
        return Math.pow(3, a) * 2
    } 
    return Math.pow(3, a)
}
console.log(maxProduct(20))


// 动态规划
/**
 * 将一个问题分解为一系列更小的子问题 并通过存储子问题的解来避免重复计算
 */

/**
 * 爬楼梯
 * 初始状态
 * 状态转移方程
 * 斐波那契数列一样
 */
let climp = (n) => {
    // 初始化 dp表 用以存储子问题的解
    if( n <= 2 ) {
        return n;
    } 
    let a = 1, b = 2;
    while(n >= 3) {
        // 2个变量滚动前进 滚动变量或者叫滚动数组
        let tmp = b;
        b = a + b;
        a = tmp;
        n--
    }
    return b;
}
console.log(climp(7), climp(6))
// 搜索 排序 分治 回溯 动态规划 贪心


/**
 * 方法一定是有步骤的
 * 权力要求书的逻辑结构还是不够清晰。
 *  对每一步的需要描述方法实现过程
 *  步骤模块不是功能模块
 *  只是一个写法，就是固定的
 *  题目
 * 
 * 权力要求书 实际上就是对发明书内容的重复 
 * 摘要说明书  
 *  中的逻辑还是不够清晰，没有关联性，比较混乱。
 * 
 * 背景技术
 *  
 */