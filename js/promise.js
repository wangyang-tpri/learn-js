/**
 * js中的promise 中的resolve和reject不需要return 直接就是一个返回值
 * 
 *promise对象表示异步操作最终的完成，不管成功还是失败 及其结果值
    .then .catch finally 都是可以链式调用的
     Promise.all()来促进异步任务的并发 在任意一个promise被拒绝时 拒绝
     Promise.any() 在任意一个promise被兑现时兑现，仅当所有的promise拒绝时才会拒绝
webpack只是一个打包工具 不会对文件进行转换的 只是一个打包工具 具体的还需要看 babel-loader
 */
let a  = new Promise((resolve) => {
    console.log ( 11 )
    resolve(20)
}, (reject) => {
    reject( 30 )
})
let b  = new Promise((resolve) => {
    console.log ( 10 )
    resolve('b')
}, (reject) => {
    reject( 30 )
})
console.log ( 2 )
setTimeout(() => {
    console.log( 4 )
}, 0);
console.log( a )
a.then((res) => {
    console.log( res )
}, (err) => {
    console.log( err )
}).finally(() => {
    console.log( 'promsie成功与否都会执行的')
})
b.then((res) => {
    console.log( res )
}, (err) => {
    console.log( err )
}).finally(() => {
    console.log( 'promsie成功与否都会执行b的')
})