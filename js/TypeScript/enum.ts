/**
 * ts中的枚举
 * enum 使用枚举可以定义一些带名字的常量
 * 使用枚举可以清晰地表达意图或创建一组有区别的用例
 */

enum Dir {
    Up = 1,
    Down,
}
console.log( Dir.Down )
/**
 * 计算和常量成员
 */
enum X { E }
console.log( X.E )
/**
 * 联合枚举与枚举成员的值
 */
