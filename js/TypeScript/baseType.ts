/**
 * typescript中的基本数据类型
 */
let isDone: boolean =  false;

let name: string = 'bob'
let list: number[] = [1, 2, 3];


/**
 * 第二种是使用数组泛型
 */
let list2: Array<number> = [3, 4];
/**
 * 元组 tuple 允许表示一个已知元素数量和类型的数组 各元素的类型不必相同
 */
let x: [string,number]
x = ['20', 10];
/**
 * 当访问一个越界的元素，会使用联合类型代替
 */

/**
 * 枚举 enum 使用枚举类型可以为一组数值赋予友好的名字
 * 枚举类型提供的便利是你可以由枚举的值得到它的名字
 */
enum Color { red ,green }
let c1: Color = Color.green;
/**
 * any
 * 编程阶段还不清楚类型的变量指定一个类型
 */

/**
 * Void
 * 表示没有任何类型 当一个函数没有返回值时
 */
function warnUser(): void {
    console.log( 'this is a error')
}

/**
 * Never
 * 表示的是哪些永不存在的值的类型
 * 返回never的函数必须存在无法达到的终点
 */
function infiniteLoop(): never{
    while( true ) {
        /**是一个无限循环 永远不会停止的 */
    }
}
/**
 * Object
 * 表示的是原始类型 
 */
declare function create( o: object | null): void;
create( { prop: 0 })

/**
 * 类型断言 有2种方式
 * 类型断言好比其他语言里的类型转换， 但是不进行特殊的数据检查和结构
 * 没有运行时影响 只是在编译阶段起作用
 * 一种是尖括号
 * 一种是as用法
 * 在jsx语法中只能使用as语法
 */
let someOne: any = 'this is ';
let num1: string  = '50'
let strLength: number = (<string>someOne).length;
let strLength2: number = (someOne as string).length;
let numLength: number = (<string>num1).length;