/**
 * 接口
 */
/**
 * typescript的核心原则之一是对值所具有的结构进行类型检查
 * 接口的作用就是为这些类型命名和为你的代码或第三方代码定义锲约
 */
function printLabel(labelObject: { label: string }) {
    console.log(labelObject.label)
}
let myObj = {
    size: 10,
    label: 'name'
}
printLabel(myObj)
/**
 * 定义了一个接口用来声明 labelValue类型
 */
interface labelValue {
    label: string
}
function printLabel2(labelObject: labelValue) {
    console.log(labelObject.label)
}

/**
 * 可选属性
 * 即给函数传入的参数对象中只有部分属性赋值了
 */
/**
 * 只读属性
 * 一些对象只能在对象刚刚创建的时候修改其值
 */

interface point {
    readonly x: number,
    readonly y: number
}
/**可以通过字面量初始化对接口point进行赋值 此后便不能再改变point中的值了 */
let p1: point = { x: 10, y: 20 }
/**ReadonlyArray<T>只是把所有可变方法去掉了，因此可以确保数组创建后再也不能被修改 */
let a: number[] = [1, 2]
let bb: ReadonlyArray<number> = a
/**
 * 判断使用readonly还是const，需要看是把它当作一个变量 还是一个属性 如果是属性的话，就是readonly 
 * 如果是变量的话 就是const
 */

/**
 * 额外的属性检查
 * 如果一个对象字面量存在任何'目标类型'不包含的属性时，会得到一个错误
 * 
 */
interface sConfig {
    color ?: string,
    width ?: number
}
function createS( config: sConfig): {color: string, area: number} {
    let newConfig = {
        color: 'white',
        area: 100
    }
    if( config.color) {
        newConfig.color = config.color
    }
    if(config.width) {
        newConfig.area = config.width * config.width
    }
    return newConfig;
}
createS({colorr: 'black'} as sConfig)

/**
 * 索引签名
 */
/**
 * 函数类型
 *  接口也可以描述函数类型接口
 *  就像是一个具有参数列表和返回类型的函数定义 参数列表里的每个参数都需要名字和类型
 */
interface searchFunc {
    (source: string): boolean
}
let mySearch: searchFunc;
mySearch = function(source: string){
    return 1==1;
}
/**
 * 对于函数类型检查来说，函数的参数名不需要与接口里定义的名字相匹配
 * 函数的参数会逐个进行检查，要求对应位置上的参数类型是兼容的 因为已经声明了函数接口了
 */
/**
 * 可索引的类型
 * 可索引类型具有一个索引签名 它描述了对象索引的类型 还有相应的索引返回值类型
 * 2种索引签名 一个是数字 一个是字符串
 *  但是数字索引的返回值必须是字符串索引返回值的子类型
 *      因为number会转换成string去索引
 */
interface StringArray {
    [index: number]: string
}
let myArr: StringArray;
myArr = ['d', 'c']





