//泛型
//组件不仅能够支持当前的数据类型，也能够支持未来的数据类型

//需要使用一种方法，使传入值和返回值的类型是相同的，此时就使用一个类型变量； 只用于表示类型而不是值

function identity<T>( arg:T ): T{
    return arg;
}
function logIdentity <T>(arg: T[]): T[] {
    console.log( arg.length )
    return arg;
}

//泛型类型
//可以使用不同的泛型参数名，只要数量和使用方式上一一对应即可

//泛型接口
interface generIndentity<T> {
    (arg: T):T
}
let myIndentity: generIndentity<number> = identity
console.log( myIndentity(10))