/**
 * ts中的枚举
 * enum 使用枚举可以定义一些带名字的常量
 * 使用枚举可以清晰地表达意图或创建一组有区别的用例
 */
var Dir;
(function (Dir) {
    Dir[Dir["Up"] = 1] = "Up";
    Dir[Dir["Down"] = 2] = "Down";
})(Dir || (Dir = {}));
console.log(Dir.Down);
