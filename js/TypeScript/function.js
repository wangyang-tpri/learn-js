//为函数定义类型
//可选参数和默认参数 就是传递给一个函数的参数必须和函数定义的参数的类型和数量是一致的
var add = function (name, num) { return "".concat(name).concat(num); };
console.log(add('today'));
console.log(add('today', 10));
