// 使用常用的面向对象的方式
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Greeter = /** @class */ (function () {
    function Greeter(greet) {
        this.name = greet;
    }
    Greeter.prototype.hello = function () {
        return "hello".concat(this.name);
    };
    Greeter.prototype.move = function (steps) {
        if (steps === void 0) { steps = 0; }
        console.log("move ".concat(steps));
    };
    return Greeter;
}());
var Animal = /** @class */ (function (_super) {
    __extends(Animal, _super);
    function Animal(name) {
        return _super.call(this, name) || this;
    }
    Animal.prototype.move = function (steps) {
        if (steps === void 0) { steps = 50; }
        console.log('重写父类的move方法，使得不同的类的同一方法具有不同的功能');
        _super.prototype.move.call(this, steps);
    };
    Animal.prototype.walk = function () {
        console.log('这是继承的子类');
    };
    return Animal;
}(Greeter));
var hello = new Greeter('YITONG');
console.log(hello.hello());
console.log(hello.move(20));
var hello2 = new Animal('today');
console.log(hello2.hello());
console.log(hello2.move());
//公共 私有和受保护的修饰符
//typescript使用的结构类型系统
//派生类有一个构造函数，必须调用super，它会执行基类的构造函数，派生类在使用this之前，必须先调用super（），这是typescipt要求的准则之一
var Snake = /** @class */ (function () {
    function Snake(tabName) {
        console.log(tabName);
        // 对类中的变量进行赋值操作
        this.name = tabName;
    }
    Snake.prototype.hello = function () {
        console.log(this.name);
    };
    return Snake;
}());
var Employee = /** @class */ (function (_super) {
    __extends(Employee, _super);
    function Employee(name, dis) {
        return _super.call(this, name) || this;
    }
    return Employee;
}(Snake));
new Employee('20', 2).dis;
new Employee('20', 2).hello();
//构造函数也是可以添加protected修饰符的，表示不可以在当前类的外部被实例化的。 但是是可以被继承的
