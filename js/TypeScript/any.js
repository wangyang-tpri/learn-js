//泛型
//组件不仅能够支持当前的数据类型，也能够支持未来的数据类型
//需要使用一种方法，使传入值和返回值的类型是相同的，此时就使用一个类型变量； 只用于表示类型而不是值
function identity(arg) {
    return arg;
}
function logIdentity(arg) {
    console.log(arg.length);
    return arg;
}
var myIndentity = identity;
console.log(myIndentity(10));
