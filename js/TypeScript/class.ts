// 使用常用的面向对象的方式

class Greeter {
    name: string;
    constructor(greet: string) {
        this.name = greet;
    }
    hello(){
        return `hello${this.name}`
    }
    move(steps: number = 0){
        console.log( `move ${steps}`)
    }
}
class Animal extends Greeter {
    constructor(name: string) {
        super(name)
    }
    move(steps = 50): void {
        console.log( '重写父类的move方法，使得不同的类的同一方法具有不同的功能')
        super.move( steps )
    }
    walk(){
        console.log( '这是继承的子类')
    }
}
let hello = new Greeter('YITONG');
console.log( hello.hello())
console.log( hello.move( 20 ))
let hello2 = new Animal('today')
console.log(hello2.hello())
console.log( hello2.move() )

//公共 私有和受保护的修饰符
//typescript使用的结构类型系统
//派生类有一个构造函数，必须调用super，它会执行基类的构造函数，派生类在使用this之前，必须先调用super（），这是typescipt要求的准则之一
class Snake {
    private name: string;
    dis: number
    protected constructor( tabName: string ) {
        console.log( tabName )
        // 对类中的变量进行赋值操作
        this.name = tabName
    }
    hello(){
        console.log( this.name )
    }
    
}
class Employee extends Snake {
    constructor(name: string, dis: number) {
        super(name)
    }
}
new Employee('20', 2).dis;
new Employee('20', 2).hello()
//构造函数也是可以添加protected修饰符的，表示不可以在当前类的外部被实例化的。 但是是可以被继承的
//抽象类 抽象类作为其他基类的派生类使用，他们一般不会被实例化


