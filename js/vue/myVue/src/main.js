// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css'
import VueI18n from 'vue-i18n'
Vue.config.productionTip = false
Vue.use(ElementUI).use(VueI18n)
import { zh } from './lang/zh'
import { en } from './lang/en'
const messages = {
	zh: {
		...zh
	},
	en: {
		...en		
	},
	
}
const i18n = new VueI18n({
	locale: localStorage.getItem('lang') || 'zh',
	messages,
})
Vue.prototype.LANG = (key, value) => i18n.$t(key, value)
new Vue({
  el: '#app',
  router,
  i18n,
  components: { App },
  template: '<App/>'
})
