/**
 * process是一个全局变量，提供了有关当前node.js进程的信息并对其进行控制
 * 进程是线程的容器，是计算机资源调度和分配的基本单位，是操作系统机构的基础
 * 由于javascript是一个单线程语言，所以通过node启动一个文件后，只有一条主进程
 * 
 */
console.log(process.pid)  
console.log(process.cwd())  // 获取当前进程所在目录
console.log(process.platform) //获取当前进程运行的操作系统平台
function foo(){
    console.log('foo')
}
process.nextTick(foo);  // 定义出一个动作，让这个 动作在下一个事件轮询的时间点行执行
console.error('bar')