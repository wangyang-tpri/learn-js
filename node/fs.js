/**
 * node中的fs模块  filesystem 该模块提供本地文件的读写能力
 *  所有与文件相关的操作都是通过fs核心模块实现的
 *  读 写 拷贝 创建文件文件夹
 */
const fs = require('fs');
fs.readFile('./1.txt', 'utf8', (err, data) => {
    if(!err) {
        console.log(data)
    }
})
/**追加写入 */
fs.appendFile('./1.txt', 'hello world', (err, data) => {
    if(!err){
        fs.readFile('./1.txt', 'utf8', (err, data) => {
            console.log(data)
        })
    }

})
/**文件拷贝 */
fs.copyFile('./1.txt', './2.txt', (err) => {
    if (!err) {
        fs.readFile('./2.txt', 'utf8', (err, data) => {
            if (!err) console.log(data)
        })
    }
})
