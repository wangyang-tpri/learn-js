/**
 * node中的buffer
 *  buffer就是在内存中开辟出一片区域，初始大小是8kb，用来存放二进制数据
 */
const fs = require('fs');
const a1 = Buffer.alloc(10)  // 创建一个大小为10字节的缓冲区
const a2 = Buffer.alloc(10, 1) // 创建一个大小为10字节的缓冲区 其中默认全部填充了值1
console.log(a1)
console.log(a2)
const b1 = Buffer.from('你好'); // 存储成二进制数据
const b2 = b1.toString();   // 可以通过toString进行转换，默认格式是utf8.如果编码和解码的格式不相同的话，则会出现乱码
console.log(b1)
console.log(b2)

/**
 * 当设定的范围导致字符串被截断的时候，也会出现乱码
 */

let len = b1.length;
console.log(len)  // 6
console.log(b1.toString('utf8', 0, 4));  // 出现了乱码 你�
console.log(b1.toString('utf8', 0, 6)); // 正常

/**
 * 应用场景
 *  I/O操作
 *  加密解密
 *  zlib.js
 */

/**
 * I/O操作  将一个文件的内容读取到另一个文件中
 */
let inputStream = fs.createReadStream('./1.txt')  // 创建可读流
let outStream = fs.createWriteStream('./2.txt')  //创建可写流
inputStream.pipe(outStream)  // 管道读写